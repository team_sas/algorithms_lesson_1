package ru.geekbrains;

import java.util.Scanner;

/**
 * Выполнил Синий Александр
 */
public class Main {

    private static Scanner scanner = new Scanner(System.in);

    /**
     * Задание 1
     * Ввести вес и рост человека. Рассчитать и вывести индекс массы тела по формуле I=m/(h*h);
     * где m-масса тела в килограммах, h - рост в метрах.
     */
    private static void calculateBMI() {
        System.out.println("Введите вес в киллограммах:");
        float m = scanner.nextFloat();

        System.out.println("Введите рост в метрах:");
        float h = scanner.nextFloat();

        float bmi = m / ( h * h );

        System.out.println("Индекс массы тела: " + bmi);
    }

    /**
     * Задание 2
     * Найти максимальное из четырех чисел. Массивы не использовать.
     */
    private static int getMaxNumber(int a, int b, int c, int d) {
        int max = (a > b) ? a : b;
        if (c > max) max = c;
        if (d > max) max = d;
        return max;
    }

    /**
     * Задание 3
     * Написать программу обмена значениями двух целочисленных переменных:
     *  a. с использованием третьей переменной;
     *  b. *без использования третьей переменной.
     */
    private static void exchangeValues() {
        // a. с использованием третьей переменной;
        int a = 1;
        int b = 2;
        int c = a;
        a = b;
        b = c;
        // b. *без использования третьей переменной.
        a = 3;
        b = 4;
        a += b;
        b = a - b;
        a = a - b;
    }

    /**
     * Задание 4
     * Написать программу нахождения корней заданного квадратного уравнения.
     */
    private static void findRootsOfQuadraticEquation(int a, int b, int c) {
        int d = b * b - 4 * a * c;

        if (d < 0) {
            System.out.println("Корней нет!");
            return;
        }

        if (d == 0) {
            int x = -b / (2 * a);
            System.out.println("x1 = x2 = " + x);
            return;
        }

        double x1 = (-b + Math.sqrt(d)) / (2 * a);
        double x2 = (-b - Math.sqrt(d)) / (2 * a);

        System.out.println("x1 = " + x1 + ", x2 = " + x2);
    }

    /**
     * Задание 5
     * С клавиатуры вводится номер месяца. Требуется определить, к какому времени года он относится.
     */
    private static void printSeasonByMonthNumber() {
        System.out.println("Введите номер месяца:");
        int month = scanner.nextInt();

        while (month < 1 || month > 12) {
            System.out.println("Введите номер месяца, целое число от 1 до 12:");
            month = scanner.nextInt();
        }

        if (month >= 3 && month <= 5) {
            System.out.println("Весна");
            return;
        }

        if (month >= 6 && month <= 8) {
            System.out.println("Лето");
            return;
        }

        if (month >= 9 && month <= 11) {
            System.out.println("Осень");
            return;
        }

        System.out.println("Зима");
    }

    /**
     * Задание 6
     * Ввести возраст человека (от 1 до 150 лет) и вывести его вместе с последующим словом «год», «года» или «лет».
     */
    private static void printAge() {
        System.out.println("Введите возраст человека:");
        int age = scanner.nextInt();

        while (age < 1 || age > 150) {
            System.out.println("Введите возраст человека, целое число от 1 до 150:");
            age = scanner.nextInt();
        }

        int num = age % 100;

        if (num >= 11 && num <= 19) {
            System.out.println(age + " лет");
            return;
        }

        num = num % 10;

        if (num == 1) {
            System.out.println(age + " год");
            return;
        }

        if (num == 2 || num == 3 || num == 4) {
            System.out.println(age + " года");
            return;
        }

        System.out.println(age + " лет");
    }

    /**
     * Задание 7
     * С клавиатуры вводятся числовые координаты двух полей шахматной доски (x1,y1,x2,y2). Требуется определить,
     * относятся ли к поля к одному цвету или нет.
     */
    private static void chessBoardCells() {
        System.out.println("Введите координаты двух полей шахматной доски через пробел (x1 y1 x2 y2):");

        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();

        if ((x1 + y1) % 2 == (x2 + y2) % 2)
        System.out.println("Клетки имеют одинаковый цвет.");
        else System.out.println("Клетки имеют разный цвет.");
    }

    /**
     * Задание 8
     * Ввести a и b и вывести квадраты и кубы чисел от a до b.
     */
    private static void squaresAndCubes() {
        System.out.println("Введите a:");
        int a = scanner.nextInt();

        System.out.println("Введите b:");
        int b = scanner.nextInt();

        if (a > b) {
            int c = a;
            a = b;
            b = c;
        }

        while (a < b - 1) {
            a++;
            int s = a * a;
            int c = s * a;
            System.out.println(a + "^2 = " + s + "; " + a + "^3 = " + c);
        }
    }

    /**
     * Задание 9
     * Даны целые положительные числа N и K. Используя только операции сложения и вычитания, найти частное
     * от деления нацело N на K, а также остаток от этого деления.
     */
    private static void division() {
        int n = 17;
        int k = 4;
        int c = 0;
        while (n > 0) {
            n = n - k;
            c++;
        }
        System.out.println("Частное от деления: " + (c - 1));
        System.out.println("Остаток от деления: " + (n + k));
    }

    /**
     * Задание 10
     * Дано целое число N (> 0). С помощью операций деления нацело и взятия остатка от деления определить,
     * имеются ли в записи числа N нечетные цифры. Если имеются, то вывести True, если нет — вывести False.
     */
    private static boolean hasEvenNumbers(int n) {
        while (n > 0) {
            int i = n % 10;
            n /= 10;
            if (i % 2 == 0) return true;
        }
        return false;
    }

    /**
     * Задание 11
     * С клавиатуры вводятся числа, пока не будет введен 0. Подсчитать среднее арифметическое всех положительных
     * четных чисел, оканчивающихся на 8.
     */
    private static void averageEventNumbers() {
        int total = 0;
        int count = 0;
        while (true) {
            System.out.println("Ввведите целое число:");
            int n = scanner.nextInt();
            if (n == 0) break;
            if (n % 10 == 8) {
                total += n;
                count++;
            }
        }
        double avg = (count == 0) ? 0 : total / count;
        System.out.println("Среднее арифметическое всех положительных четных чисел, оканчивающихся на 8: " + avg);
    }

    /**
     * Задание 12
     * Написать функцию нахождения максимального из трех чисел.
     */
    private static int getMaxNumber(int a, int b, int c) {
        int max = a;
        if (b > max) max = b;
        if (c > max) max = c;
        return max;
    }
}
